onebit
=======

A Symfony project created on August 31, 2017, 5:09 pm.
___

czyszczenie pamięci podręcznej
=
php bin/console cache:clear --no-warmup --env=prod

php bin/console cache:clear --no-warmup --env=dev

Generowanie bazy z modelu
=
php bin/console doctrine:schema:update --force

Wersja developerska pod urrlem
=
http://onebit.pl/app_dev.php

Pobieranie danych o spółkach
=
php ./bin/console app:download-data-all
<?php

namespace AppBundle\Controller;

use AppBundle\Menagers\MenagerCompanyDownload;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\WatchedMovie;
use AppBundle\Entity\Rating;
use AppBundle\Entity\Year;
use AppBundle\Entity\Actor;
use AppBundle\Entity\Genre;
use AppBundle\Entity\Directory;
use AppBundle\Entity\Country;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;


use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Form\ActorType;
use AppBundle\Form\DirectoryType;



class MovieController extends Controller
{
    /**
     * Lista wszystkich spułek z aktualnym kursem
     * @Route("/movie/list", name="movie-list")
     */
    public function listMovieAction(Request $request)
    {
     //   $menager = new MenagerCompanyDownload();
     //   $menager->run($this->getDoctrine());

        $repository = $this->getDoctrine()->getRepository(WatchedMovie::class)->findAll();

        // replace this example code with whatever you need
        return $this->render('movie/listMovie.html.twig', [
            'movies'        => $repository
        ]);
    }

    /**
     * Formularz do dodawania
     * @Route("/movie/new", name="movie-new")
     */
    public function newAction(Request $request)
    {
     //   $menager = new MenagerCompanyDownload();
     //   $menager->run($this->getDoctrine());
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();

        $watchedMovie = new WatchedMovie();

        $watchedMovie->addEmptyActor();
        $watchedMovie->addEmptyDirectory();

        $form = $this->generateWatchinMoveForm($watchedMovie);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $dataWatchedMovie = $form->getData();

            $dataWatchedMovie->setUniqueActors($this->getDoctrine());
            $dataWatchedMovie->setUniqueDirectors($this->getDoctrine());
            $dataWatchedMovie->setUniqueRating($this->getDoctrine());
            $dataWatchedMovie->setUniqueYear($this->getDoctrine());

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
             $entityManager = $this->getDoctrine()->getManager();
             $entityManager->persist($dataWatchedMovie);
             $entityManager->flush();

            return $this->redirectToRoute('movie-list');
        }

        return $this->render('movie/newMovie.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Formularz do edycji
     * @Route("/movie/edit/{idWatchedMovie}", name="movie-edit", requirements={"idWatchedMovie"="\d+"})
     */
    public function editAction(Request $request, $idWatchedMovie)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
    //    $menager = new MenagerCompanyDownload();
     //   $menager->run($this->getDoctrine());

        $watchedMovie = $this->getDoctrine()
            ->getRepository(WatchedMovie::class)
            ->find($idWatchedMovie);

        if (!$watchedMovie) {
            throw $this->createNotFoundException(
                'No watchedMovie found for id ' . $idWatchedMovie
            );
        }

        $form = $this->generateWatchinMoveForm($watchedMovie);
        $form->handleRequest($request);

        if ( $form->isValid()) {

            $dataWatchedMovie = $form->getData();

            $dataWatchedMovie->setUniqueActors($this->getDoctrine());
            $dataWatchedMovie->setUniqueDirectors($this->getDoctrine());
            $dataWatchedMovie->setUniqueRating($this->getDoctrine());
            $dataWatchedMovie->setUniqueYear($this->getDoctrine());

            // aktualizujemy dane

            $entityManager = $this->getDoctrine()->getManager();

            //tutaj chyba jest bład, $dataWatchedMovie nie potrzebne
            $entityManager->flush($dataWatchedMovie);


            return $this->redirectToRoute('movie-list');
        }


        return $this->render('movie/editMovie.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @param $WatchedMovie
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    private function generateWatchinMoveForm($WatchedMovie)
    {
        return $this->createFormBuilder($WatchedMovie)
            ->add('title', TextType::class)

            ->add('actors', CollectionType::class, array(
                'entry_type' => ActorType::class,
                'entry_options' => array('label' => true),
                'allow_add' => true,

            ))
            ->add('addActor', ButtonType::class, array(
                'attr' => array('class' => 'add-actor'),
                'label' => 'Dodaj aktora',
            ))
            ->add('directors', CollectionType::class, array(
                'entry_type' => DirectoryType::class,
                'entry_options' => array('label' => true),
                'allow_add' => true
            ))
            ->add('addDirector', ButtonType::class, array(
                'attr' => array('class' => 'add-director'),
                'label' => 'Dodaj reżysera',
            ))
            ->add('year', EntityType::class, array(
                'class' => Year::class,
                'choice_label' => function ($year) {
                    return $year->getYear();
                }
            ))
            ->add('Countries', EntityType::class, [
                'multiple' => true,
                // Render as checkboxes
                'expanded' => true,
                'class'    => Country::class,
                'mapped' => true,
                'choice_label' => function ($category) {
                    return $category->getCountry();
                }
            ])
            ->add('Genries', EntityType::class, [
                'multiple' => true,
                // Render as checkboxes
                'expanded' => true,
                'class'    => Genre::class,
                'mapped' => true,
                'choice_label' => function ($genre) {
                    return $genre->getGenre();
                }
            ])
            ->add('rating', EntityType::class, array(
                'class' => Rating::class,
                'choice_label' => function ($rating) {
                    return $rating->getRating();
                }
            ))
            ->add('save', SubmitType::class, array('label' => 'Create Task'))
            ->getForm();
    }
}
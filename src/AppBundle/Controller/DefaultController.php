<?php

namespace AppBundle\Controller;

use AppBundle\DataFinancialCalculator\CalculatorAttractiveCompany;
use AppBundle\Entity\Menu;
use AppBundle\Entity\WatchedMovie;
use AppBundle\Menagers\MenagerCompanyDownload;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    /**
     * Lista wszystkich spułek z aktualnym kursem
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

      //  $menager = new MenagerCompanyDownload();
       // $menager->run($this->getDoctrine());

       // $calculator = new CalculatorAttractiveCompany();
     //   $calculator->calculateNameCompany( $this->getDoctrine(), 'CITYSERV');
     //   $calculator->calculateAll( $this->getDoctrine());

        $ListaSpulek = array(
            "Action" => "3.3",
            "Atende" => "3.4",
            "Echo" => "5",
            "Lena" => "3.4",
        );
      //  $test = new \ReflectionClass($dataProviderCompany);
     //   \Reflection::export($test);
        $repository = $this->getDoctrine()->getRepository(WatchedMovie::class)->findAll();
        $auth_checker = $this->get('security.authorization_checker');
		$token = $this->get('security.token_storage')->getToken();
	//	var_dump($token->getUser());
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'ListaSpulek'   => $ListaSpulek,
            'movies'        => $repository,
        ]);
    }

    public function menu($max = 3)
    {
        //pobieramy dane z bazy
        $allMenu = $this->getDoctrine()
       ->getRepository(Menu::class)
       ->findAll();

        $menu = [];
        foreach ($allMenu as $singleMenu) {
            $menu[$singleMenu->getName()] = $this->generateUrl($singleMenu->getUrl());
        }

        // wyswietlamy dane w widoku
        return $this->render('default/menu.html.twig', [
            'menu'   => $menu,
        ]);
    }



}

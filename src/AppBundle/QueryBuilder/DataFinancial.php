<?php
namespace AppBundle\QueryBuilder;

class DataFinancial
{
    public static function createQuery($repository, $dataCompany)
    {
        return $repository->createQueryBuilder('p')
            ->leftJoin('p.quarter', 'quarter')
            ->leftJoin('p.year', 'year')
            ->where('quarter.description = :quarter')
            ->andwhere(' year.year = :year')
            ->andwhere('p.company = :idCompany')
            ->setParameter('idCompany', $dataCompany->getCompany()->getId())
            ->setParameter('quarter', $dataCompany->getQuarter()->getDescription())
            ->setParameter('year', $dataCompany->getYear()->getYear())
            ->getQuery();
    }
}
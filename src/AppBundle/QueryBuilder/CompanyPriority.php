<?php
namespace AppBundle\QueryBuilder;

use AppBundle\Entity\Company;

class CompanyPriority
{
    public static function noDataFinancial($doctrine, $idTypeData)
    {
        $query = $doctrine->getRepository(Company::class);

        return $query
            ->createQueryBuilder('c')
            ->select('c')
            ->leftJoin(
                'AppBundle\Entity\PriorityDownload', 'p', \Doctrine\ORM\Query\Expr\Join::WITH, 'p.company = c.id AND p.type_data = ' . $idTypeData
            )
            ->where('p.company IS NULL')
            ->getQuery()
            ->getResult();
    }
}
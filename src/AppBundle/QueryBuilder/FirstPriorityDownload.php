<?php

namespace AppBundle\QueryBuilder;

use AppBundle\Entity\PriorityDownload;

class FirstPriorityDownload
{
    public static function get($doctrine)
    {
        return $doctrine->getRepository(PriorityDownload::class)
            ->createQueryBuilder('p')
            ->where('p.priority > :priority')
            //   ->andwhere('p.company = 1328')
            ->setParameter('priority', 0)
            //   ->setParameter('typeData', [1, 2, 3, 5, 7, 8, 9, 10, 11, 12])
            ->orderBy('p.priority', 'DESC')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }

    public static function getAll($doctrine)
    {
        return $doctrine->getRepository(PriorityDownload::class)
            ->createQueryBuilder('p')
            ->where('p.priority > :priority')
            ->setParameter('priority', 0)
            ->getQuery()
            ->getResult();
    }

}
<?php

namespace AppBundle\DataFinancialCalculator\Calculators\Interfaces;

interface Calculator
{
    public function notMeetsRequirement();
    public function calculate();

}
<?php

namespace AppBundle\DataFinancialCalculator\Calculators;


use AppBundle\DataFinancialCalculator\Calculators\Interfaces\Calculator;
use AppBundle\Entity\Dividend;
use AppBundle\ObjectProvider\DataTimeProvider;


class DividendCalculate implements Calculator
{
    public $minDividend = null;
    public $dividends = null;
    public $company = null;


    public function __construct($minDividend, $company, $doctrine)
    {
        $this->company = $company;
        $this->minDividend = $minDividend;
        $year = new DataTimeProvider();
        $year = $year->get('-5 year');
        $year = $year->format("Y");
        $date = $year . '-01-01 00:00:00';

        $this->dividends = $doctrine->getRepository(Dividend::class)
            ->createQueryBuilder('dividend')
            ->where('dividend.date_payment > :lastFiveYear')
            ->andwhere('dividend.company = :idCompany')
            ->setParameter('lastFiveYear', $date)
            ->setParameter('idCompany', $company->getId())
            ->orderBy('dividend.date_payment', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function notMeetsRequirement()
    {

     //   var_dump(count($this->dividends));
        if (count($this->dividends) >= $this->minDividend ) {
            return false;
        }


        return true;
    }

    public function calculate()
    {

        $dividends = [];
        $sum =0;
        foreach ($this->dividends as $dividend) {
            if (is_null($dividends[$dividend->getDatePayment()->format("Y")] ?? null)) {
                $dividends[$dividend->getDatePayment()->format("Y")] = $dividend->getPaycheck();
            } else {
                $dividends[$dividend->getDatePayment()->format("Y")] += $dividend->getPaycheck();
            }

            $sum += $dividend->getPaycheck();
        }

        $isProgress = true;
        $lastPrice = 0;
        foreach ($dividends as $dividend) {
            if ($lastPrice <= ((float) $dividend)) {
                $lastPrice = ((float) $dividend);
            } else {
                $isProgress = false;
                break;
            }
        }
        $exchange = $this->company->getExchange() * 100;
        $sum = $sum * 100;
        $percent = (int) ($sum *100 / $exchange);

        if ($isProgress && count($dividends) > 0) {
            $percent += 10;
        }

        return $percent;
    }
}
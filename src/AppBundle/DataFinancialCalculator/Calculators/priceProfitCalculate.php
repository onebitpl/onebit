<?php

namespace AppBundle\DataFinancialCalculator\Calculators;

use AppBundle\DataFinancialCalculator\Calculators\Interfaces\Calculator;
use AppBundle\Entity\NetProfit;
use AppBundle\ObjectProvider\DataTimeProvider;

class priceProfitCalculate implements Calculator
{
    public function __construct($minCountPriceProfit, $company, $doctrine)
    {
        $this->company = $company;
        $this->minCountPriceProfit = $minCountPriceProfit;
        $year = new DataTimeProvider();
        $year = $year->get('-1 year');
        $year = $year->format("Y");

        $this->dividends = $doctrine->getRepository(NetProfit::class)
            ->createQueryBuilder('net_profit')
            ->where('net_profit.year >= :idYear')
            ->andwhere('net_profit.company = :idCompany')
            ->setParameter('idYear', $year)
            ->setParameter('idCompany', $company->getId())
            ->addOrderBy('net_profit.year', 'DESC')
            ->addOrderBy('net_profit.quarter', 'DESC')
            ->getQuery()
            ->setMaxResults(4)
            ->getResult();

        $this->dividends = array_reverse($this->dividends);

    }
    public function notMeetsRequirement()
    {
        $counter = 0;
        $sum = 0;
        foreach($this->dividends as $netPrice) {
            if ($netPrice->getValue() > 0) {
                $counter++;
            }

            $sum += $netPrice->getValue();
        }

        if ($sum > 0 && $counter >= $this->minCountPriceProfit) {
            return false;
        } else {
            return true;
        }
    }

    public function calculate()
    {
        $counter = 0;
        $lastValue = 0;
        $isProgress = true;

        foreach($this->dividends as $netPrice) {
            if ($netPrice->getValue() > 0) {
                $counter++;
            }

            if ($lastValue > $netPrice->getValue()) {
                $isProgress = false;
            }

            $lastValue = $netPrice->getValue();
        }

        if ($isProgress) {
            $counter += 4;
        }

        if ($counter < $this->minCountPriceProfit) {
            return 0;
        }

        return ($counter*5);
    }
}
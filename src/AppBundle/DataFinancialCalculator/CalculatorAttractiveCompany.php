<?php

namespace AppBundle\DataFinancialCalculator;

use AppBundle\DataFinancialCalculator\Calculators\DividendCalculate;
use AppBundle\DataFinancialCalculator\Calculators\priceProfitCalculate;
use AppBundle\Entity\Company;

class CalculatorAttractiveCompany
{
    private $entityManager;
    public function calculateAll($doctrine)
    {
        $this->entityManager = $doctrine->getManager();
        $companies = $doctrine
            ->getRepository(Company::class)
            ->findBy(array(
                'name_company' => 'NWAI'
            ));

        foreach ($companies as $company) {
            $this->calculateCompany($company, $doctrine);
        }

        $this->entityManager->flush();
    }

    public function calculateNameCompany($doctrine, $name)
    {
        $this->entityManager = $doctrine->getManager();
        $companies = $doctrine
            ->getRepository(Company::class)
            ->findBy(array(
                      'name_company' => $name
            ));

        foreach ($companies as $company) {
            $this->calculateCompany($company, $doctrine);
        }

        $this->entityManager->flush();
    }

    public function calculateCompany($company, $doctrine)
    {
        $calculators = [];

        $calculators[] = new DividendCalculate(3, $company, $doctrine);
        $calculators[] = new PriceProfitCalculate(3, $company, $doctrine);
     //   $calculators[] = new PriceAssetValueCalculate();
     //   $calculators[] = new PriceNettoCalculate();

        $attractive = 0;
        $meetsRequirement = true;
        foreach ($calculators as $calculator) {
            if ( $calculator->notMeetsRequirement()) {
                $meetsRequirement = false;
            }

            $attractive += $calculator->calculate();
        }

        $company->setAttractive($attractive);
        $company->setMeetsRequirement($meetsRequirement);

        $this->entityManager->persist($company);
    }
}
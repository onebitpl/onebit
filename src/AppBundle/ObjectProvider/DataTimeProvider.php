<?php

namespace AppBundle\ObjectProvider;

class DataTimeProvider
{
    public static function getNow()
    {
        return new \DateTime('now');
    }

    public static function get($shift)
    {
        $dateImmutable = self::getNow();
        $dateImmutable->modify($shift);

        return $dateImmutable;
    }
}
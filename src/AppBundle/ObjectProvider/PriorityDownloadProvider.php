<?php

namespace AppBundle\ObjectProvider;

use AppBundle\Entity\PriorityDownload;

class PriorityDownloadProvider
{
    public static function get($company, $typeData)
    {
        $dateImmutable = DataTimeProvider::get('-1 year');

        $priorityDownload = new PriorityDownload();
        $priorityDownload->setCompany($company);
        $priorityDownload->setTypeData($typeData);
        $priorityDownload->setLastDownload($dateImmutable);
        $priorityDownload->setPriority(0);

        return $priorityDownload;
    }
}
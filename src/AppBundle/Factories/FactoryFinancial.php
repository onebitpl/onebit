<?php

namespace AppBundle\Factories;


use AppBundle\Entity\Amortization;
use AppBundle\Entity\Assets;
use AppBundle\Entity\Ebitda;
use AppBundle\Entity\GrossProfit;
use AppBundle\Entity\NetIncome;
use AppBundle\Entity\NetProfit;
use AppBundle\Entity\EquityCapital;
use AppBundle\Entity\CountStockShare;
use AppBundle\Entity\ProfitPerShare;
use AppBundle\Entity\BookValueShare;


class FactoryFinancial
{
    static public function create(string $typeFinancial) : object
    {
        $object = null;

        switch ($typeFinancial) {
            case 'Przychody z tytułu odsetek':
            case 'Przychody z tytułu prowizji':
            case 'Przychody netto ze sprzedaży':
                $object = new NetIncome();
                break;

            case 'Zysk (strata) brutto':
                $object = new GrossProfit();
                break;

            case 'Zysk (strata) netto':
                $object = new NetProfit();
                break;

            case 'Amortyzacja':
                $object = new Amortization();
                break;

            case 'EBITDA':
                $object = new Ebitda();
                break;

            case 'Aktywa':
                $object = new Assets();
                break;

            case 'Kapitał własny':
                $object = new EquityCapital();
                break;

            case 'Liczba akcji':
                $object = new CountStockShare();
                break;

            case 'Zysk na akcję':
                $object = new ProfitPerShare();
                break;

            case 'Wartość księgowa na akcję':
                $object = new BookValueShare();
                break;

            default:
                throw new \Exception('niezidentyfikowany obiekt ' . $typeFinancial);

        }

        return $object;
    }
}
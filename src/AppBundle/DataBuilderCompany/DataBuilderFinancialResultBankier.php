<?php

namespace AppBundle\DataBuilderCompany;

use AppBundle\Curl\CurlHelper;
use AppBundle\DataProvider\DataProviderWeb;
use AppBundle\DataBuilderCompany\Interfaces\DataBuilderFinancialResultInterface;
use AppBundle\Entity\Quarter;
use AppBundle\Entity\Year;
use AppBundle\Factories\FactoryFinancial;

use AppBundle\Services\TextModifier;
use Exception;

class DataBuilderFinancialResultBankier implements DataBuilderFinancialResultInterface
{
    private $url = null;
    private $urlPage = null;
    private $companyName = null;
    private $company = null;
    private $dataHead = [];
    private $infoControl = null;


    public function __construct()
    {
    }

    public function setInfo ($infoControl)
    {
        $this->infoControl = $infoControl;
        $this->setNameCompany($this->infoControl->getCompany());
    }

    public function getResult()
    {
        if ($this->infoControl->getPriorityDownloads()->getLastPagination() === null) {
            $this->infoControl->setCountPagination($this->getCountPage());
        }

        if ($this->infoControl->getPriorityDownloads()->getLastPagination() > 0) {
            $result = $this->getFinancialResultFromPage($this->infoControl->getPriorityDownloads()->getLastPagination());
            $this->infoControl->setCountPagination($this->infoControl->getPriorityDownloads()->getLastPagination()-1);
        } else {
            $result = $this->getFinancialResult();
        }

        return $result;
    }

    public function setNameCompany($company)
    {
        $this->company = $company;
        $this->companyName = $company->getNameCompany();

        $url = 'skonsolidowany';
        if ( ! $company->getIsConsolidation()) {
            $url = 'jednostkowy';
        }

        $this->url = str_replace('%company%', $this->companyName, 'https://www.bankier.pl/gielda/notowania/akcje/%company%/wyniki-finansowe/%url%/kwartalny/standardowy/');
        $this->url = str_replace('%url%', $url, $this->url);
        $this->urlPage = str_replace('%company%', $this->companyName, 'https://www.bankier.pl/gielda/notowania/akcje/%company%/wyniki-finansowe');


    }

    public function getNameCompany()
    {
        return $this->companyName;
    }

    public function getFinancialResult()
    {
       return $this->getFinancialResultFromPage();
    }

    public function getFinancialResultFromPage($page = 1)
    {
        $objectFinancial = [];

        $curl = new CurlHelper();
        $curl->downloadWeb($this->url . $page);

        $dataProvider = new DataProviderWeb($curl->getHtml());
        $head = $dataProvider->getValueBySelect('.boxBlue table thead tr th');

        $countColumn = count($head);
//

        $this->setDataHead($head);
            $dataFinancial = [];
            $recordNumber = 0;
            $cellNumber = 0;
            foreach ($dataProvider->getValueBySelect('.boxBlue table tbody tr td') as $record) {
                $dataFinancial[$recordNumber][$cellNumber] = $record;
                $cellNumber++;
                if ($cellNumber >= $countColumn ) {
                    $recordNumber++;
                    $cellNumber = 0;
                }
            }


            foreach ($dataFinancial as $singleDataFinancial) {
                if($singleDataFinancial[0] == 'Waluta') continue;
                if($singleDataFinancial[0] == 'Raport zbadany przez audytora') continue;
                if($singleDataFinancial[0] == 'Zysk (strata) z działal. oper. (tys.)') continue;
                if($singleDataFinancial[0] == 'Wynik na działalności bankowej (tys.)') continue;
                if($singleDataFinancial[0] == 'Składka na udziale własnym (tys.)') continue;
                if($singleDataFinancial[0] == 'Przychody z lokat (tys.)') continue;
                if($singleDataFinancial[0] == 'Wynik techniczny ubezpieczeń majątkowych i osobowych (tys.)') continue;



                for ($cell = 1; $cell <= ($countColumn-1); $cell++) {
                    $objectFinancial[] = $this->createNewFinancial($singleDataFinancial, $cell);
                }
            }

        return $objectFinancial;
    }

    public function getCountPage()
    {
        $curl = new CurlHelper();
        $curl->downloadWeb($this->urlPage);

        $dataProvider = new DataProviderWeb($curl->getHtml());
        $pagination = $dataProvider->getValueBySelect('.pagination .numeral:last-child');

        if (empty($pagination)) {
            return 0;
        }

        if(strpos($dataProvider->getAttributeBySelect('.pagination .numeral:last-child')[0],'skonsolidowany')) {
            $this->company->setIsConsolidation(1);
        }

        $this->setNameCompany($this->company);

        return $pagination[0];
    }

    private function createNewFinancial($record, $cell)
    {
        $quarter = new Quarter();

        $quarter->setDescription($this->dataHead[$cell]['quarter'] . ' kwartał');

        $year = new Year();
        $year->setYear($this->dataHead[$cell]['year']);

        $record[0] = trim(str_replace(['(zł)','(tys. szt.)',' (tys.)*',' (tys.)', ' (tys. zł)'], '',$record[0]));
        $Financial = FactoryFinancial::create($record[0]);

        $Financial->setValue(TextModifier::removeWhiteSpace($record[$cell]));
        $Financial->setYear($year);
        $Financial->setQuarter($quarter);

        return $Financial;
    }

    private function setDataHead(array $head)
    {
        $indexData = 1;
        foreach ($head as $singleHead) {
            $explodeSingleHead = explode(' ', $singleHead);

            if (count($explodeSingleHead) > 1 && $explodeSingleHead[1] == '0000') {
                $this->dataHead[$indexData]['quarter'] = 'I';
                $this->dataHead[$indexData]['year'] = 1955;
                $indexData++;
                continue;
            }

            $year = $explodeSingleHead[2] ?? null;
            if (is_null($year)) continue;
            $this->dataHead[$indexData]['quarter'] = $explodeSingleHead[0];
            $this->dataHead[$indexData]['year'] = $year;

            $indexData++;
        }

    }
}
<?php

namespace AppBundle\DataBuilderCompany;

use AppBundle\Curl\CurlHelper;
use AppBundle\DataProvider\DataProviderWeb;
use AppBundle\DataBuilderCompany\Interfaces\DataBuilderCompanyInterface;

use AppBundle\Entity\Company;
use Exception;

class DataBuilderCompanyBankier implements DataBuilderCompanyInterface
{
    private $url = '';
    private $isGpw = 0;
    private $infoControl = null;

    public function __construct()
    {
    }

    public function setInfo ($infoControl)
    {
        $this->infoControl = $infoControl;
        $this->setGpw($this->infoControl->getCompany()->getIsGpw());
    }

    public function getResult()
    {
        return $this->getCompanies();
    }

    public function setGpw ($gpw) {
        if ($gpw) {
            $this->isGpw = true;
            $this->url = 'https://www.bankier.pl/gielda/notowania/akcje';
        } else {
            $this->isGpw = false;
            $this->url = 'https://www.bankier.pl/gielda/notowania/new-connect';
        }
    }


    public function getCompanies()
    {
        $companies = [];

        $curl = new CurlHelper();
        $curl->downloadWeb($this->url);

        $dataProvider = new DataProviderWeb($curl->getHtml());
        $head = $dataProvider->getValueBySelect('.sortTable thead tr th');

        if ($this->haveAppropriateColumns($head)) {
            foreach ($dataProvider->getValueBySelect('.sortTable tbody tr') as $record) {

                $cells = explode(' ', $record);

                if ($this->notHaveData($cells)) continue;

                $companies[] = $this->createNewCompany($cells);
            }

        }

        return $companies;
    }

    private function createNewCompany($cells)
    {
        $company = new Company();
        $exchange = preg_replace('/\h/u','',$cells[22]);
        $exchange = str_replace([',',' '],['.',''],$exchange);

        $company->setIsGpw($this->isGpw);
        $company->setNameCompany(trim($cells[0]));
        $company->setExchange($exchange);

        return $company;
    }

    private function haveAppropriateColumns (array $head)
    {
        if (trim(str_replace('AD','', $head[0])) == 'Walor') {
            if (trim(str_replace('AD','', $head[1])) == 'Kurs') {
                return true;
            }
        }

        return false;
    }

    private function notHaveData (array $cells) {
        return ! array_key_exists(22, $cells);
    }

}
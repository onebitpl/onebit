<?php

namespace AppBundle\DataBuilderCompany\Interfaces;

interface DataBuilderFinancialResultInterface
{
    public function getFinancialResult();
    public function setInfo($infoControl);
    public function getResult();
}
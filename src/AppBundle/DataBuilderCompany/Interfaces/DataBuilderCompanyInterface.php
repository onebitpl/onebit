<?php

namespace AppBundle\DataBuilderCompany\Interfaces;

interface DataBuilderCompanyInterface
{
    public function getCompanies();
    public function setInfo($infoControl);
    public function getResult();
}
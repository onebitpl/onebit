<?php

namespace AppBundle\DataBuilderCompany\Interfaces;

interface DataBuilderDividendInterface
{
    public function getDividends();
    public function setInfo($infoControl);
    public function getResult();
}
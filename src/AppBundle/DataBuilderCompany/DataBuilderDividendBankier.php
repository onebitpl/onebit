<?php

namespace AppBundle\DataBuilderCompany;

use AppBundle\Curl\CurlHelper;
use AppBundle\DataProvider\DataProviderWeb;
use AppBundle\DataBuilderCompany\Interfaces\DataBuilderDividendInterface;
use AppBundle\Entity\Dividend;

use Exception;

class DataBuilderDividendBankier implements DataBuilderDividendInterface
{
    private $url = null;
    private $companyName = null;
    private $infoControl = null;

    public function __construct()
    {
    }

    public function setInfo ($infoControl)
    {
        $this->infoControl = $infoControl;
        $this->setNameCompany($this->infoControl->getNameCompany());;
    }

    public function getResult()
    {
        return $this->getDividends();
    }

    public function getNameCompany()
    {
        return $this->companyName;
    }


    public function getDividends()
    {

        $dyvidends = [];

        $curl = new CurlHelper();
        $curl->downloadWeb($this->url);

        $dataProvider = new DataProviderWeb($curl->getHtml());
        $head = $dataProvider->getValueBySelect('.box945:nth-child(6) #gemiusHitSection16 table thead tr th');

        if ($this->haveAppropriateColumns($head)) {
            $dataDividend = [];
            $recordNumber = 0;
            $cellNumber = 0;
            foreach ($dataProvider->getValueBySelect('.box945:nth-child(6) #gemiusHitSection16 table tbody tr td') as $record) {
                $dataDividend[$recordNumber][$cellNumber] = $record;
                $cellNumber++;
                if ($cellNumber >= 3 ) {
                    $recordNumber++;
                    $cellNumber = 0;
                }
            }
          //  var_dump($dataDividend);
            foreach ($dataDividend as $record) {
                if($record[1] != 'Wypłata dywidendy') continue;
            //        var_dump($record);
           //     echo '<br>';
                $dyvidends[] = $this->createNewDividend($record);
            }

        }

        return $dyvidends;
    }

    public function setNameCompany($nameCompany)
    {
        $this->companyName = $nameCompany;
        $this->url = str_replace('%nameCompany%', $nameCompany, 'https://www.bankier.pl/gielda/notowania/akcje/%nameCompany%/akcjonariat');
    }

    private function createNewDividend($record)
    {
        $dyvidend = new Dividend();

        $paycheck = str_replace('Dywidenda na akcję:','',$record[2]);
        $paycheck = str_replace('zł','',$paycheck);
        $paycheck = str_replace(',','.',$paycheck);

        $paycheck = trim($paycheck);

        $dyvidend->setPaycheck($paycheck);
        $date = strtotime($record[0]);
        $dyvidend->setDatePayment(date('Y-m-d h:i:s', $date));

        return $dyvidend;
    }

    private function haveAppropriateColumns (array $head)
    {
        if (trim($head[0]) == 'Data operacji') {
            if (trim($head[1]) == 'Rodzaj operacji') {
                if (trim($head[2]) == 'Komentarz') {
                    return true;
                }
            }
        }

        return false;
    }
}
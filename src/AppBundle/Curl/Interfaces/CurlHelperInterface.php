<?php

namespace AppBundle\Curl\Interfaces;

interface CurlHelperInterface
{
    public function getHtml();
    public function getHeader();
    public function getStatus();
    public function downloadWeb($url);
}
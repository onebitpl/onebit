<?php

namespace AppBundle\Curl;

use AppBundle\Curl\Interfaces\CurlHelperInterface;

class CurlHelper implements CurlHelperInterface
{
    private $html;
    private $header;
    private $status;

    public function downloadWeb($url)
    {

        //@todo do refaktoryzacji
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
      //  curl_setopt($curl, CURLOPT_HEADER, true);
      //  curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getHeader());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        
        $this->setHtml(curl_exec($curl));
        
        //sprawdzi� co ta metoda dok�adnie robi i wrzyci� wynik w zmienn�
        if (!curl_errno($curl)) {
            $this->setStatus(curl_getinfo($curl, CURLINFO_HTTP_CODE));
        } else {
            echo 'Curl error: ' . curl_error($curl);
        }

        curl_close($curl);
    }

    public function getHtml()
    {
        return $this->html;
    }

    public function setHtml($html)
    {
        $this->html = $html;
    }


    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public Function getHeader()
    {
        if (is_null($this->header))
        {
            $this->setHeader();
        }

        return $this->header;
    }

    private Function setHeader($header = null)
    {
        if (is_null($header))
        {
            $header = [];
            foreach (getallheaders() as $typeHeader => $singleHeader) {
                $header[] = "$typeHeader: $singleHeader";
            }
        }

        $this->header = $header;

        return $this->header;
    }
}
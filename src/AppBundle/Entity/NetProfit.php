<?php

namespace AppBundle\Entity;

use AppBundle\QueryBuilder\DataFinancial;
use Doctrine\ORM\Mapping as ORM;

/**
 * zysk netto
 *
 * @ORM\Entity
 * @ORM\Table(name="net_profit")
 */
class NetProfit
{
    /**
     * @ORM\Column(type="integer", name="id_net_profit")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Company", cascade="persist")
     * @ORM\JoinColumn(name="id_company", referencedColumnName="id_company")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="Year", cascade="persist")
     * @ORM\JoinColumn(name="id_year", referencedColumnName="id_year")
     */
    private $year;

    /**
     * @ORM\ManyToOne(targetEntity="Quarter", cascade="persist")
     * @ORM\JoinColumn(name="id_quarter", referencedColumnName="id_quarter")
     */
    private $quarter;

    /**
     * @ORM\Column(type="integer", length=10)
     */
    private $net_profit;





    /**
     * Default constructor, initializes collections
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return mixed
     */
    public function getNetProfit()
    {
        return $this->net_profit;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->net_profit;
    }

    /**
     * @param mixed
     */
    public function getQuarter()
    {
        return $this->quarter;
    }

    /**
     * @param mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @param mixed $netProfit
     */
    public function setNetProfit($netProfit)
    {
        $this->net_profit = $netProfit;
    }

    /**
     * @param mixed $netProfit
     */
    public function setValue($netProfit)
    {
        $this->net_profit = $netProfit;
    }


    /**
     * @param mixed $quarter
     */
    public function setQuarter($quarter)
    {
        $this->quarter = $quarter;
    }

    public function setYear($year)
    {
        $this->year = $year;
    }

    public function findOrCreateFinancialResult($doctrine)
    {
        $netProfit = $this->getFromBase($doctrine);

        if (is_null($netProfit)) {
            $netProfit = new NetProfit();
        }

        return $netProfit;
    }

    public function getFromBase($doctrine)
    {
        $repository = $doctrine
            ->getRepository(NetProfit::class);

        $query = DataFinancial::createQuery($repository, $this);

        return $query->setMaxResults(1)->getOneOrNullResult();
    }

}

<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="rating")
 */
class Rating
{
    /**
     * @ORM\Column(type="integer", name="id_rating")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", length=2)
     */
    private $rating;

    public function getRating() 
    {
        return $this->rating;
    }
    public function setRating($rating) 
    {
        return $this->rating = $rating;
    }
    public function setIdRating($id) 
    {
        $this->id = $id;
    }
    public function getId() 
    {
        return $this->id;
    }
}
<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="available_builder")
 */
class AvailableBuilder
{
    /**
     * @ORM\Column(type="integer", name="id_available_builder")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=120)
     */
    private $builder_name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getBuilderName()
    {
        return $this->builder_name;
    }

    /**
     * @param mixed $builder_name
     */
    public function setBuilderName($builder_name): void
    {
        $this->builder_name = $builder_name;
    }



}
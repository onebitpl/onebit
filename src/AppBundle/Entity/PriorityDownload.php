<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="priority_download")
 */
class PriorityDownload
{
    /**
     * @ORM\Column(type="integer", name="id_priority_download")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Company", cascade="persist")
     * @ORM\JoinColumn(name="id_company", referencedColumnName="id_company")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="TypeData", cascade="persist")
     * @ORM\JoinColumn(name="id_type_data", referencedColumnName="id_type_data")
     */
    private $type_data;


    /**
     * @ORM\Column(type="datetime")
     */
    private $last_download;

    /**
     * @ORM\Column(type="integer", length=5)
     */
    private $priority;

    /**
     * @ORM\Column(type="integer", length=5, nullable=true, )
     */
    private $last_pagination;

    /**
     * Default constructor, initializes collections
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTypeData()
    {
        return $this->type_data;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return mixed
     */
    public function getLastDownload()
    {
        return $this->last_download;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @return mixed
     */
    public function getLastPagination()
    {
        return $this->last_pagination;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @param mixed $type_data
     */
    public function setTypeData($type_data): void
    {
        $this->type_data = $type_data;
    }

    /**
     * @param mixed $last_download
     */
    public function setLastDownload($last_download): void
    {
        $this->last_download = $last_download;
    }

    /**
     * @param mixed $priority
     */
    public function setPriority($priority): void
    {
        $this->priority = $priority;
    }

    /**
     * @param mixed $last_pagination
     */
    public function setLastPagination($last_pagination): void
    {
        $this->last_pagination = $last_pagination;
    }
}

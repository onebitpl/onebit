<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="actor")
 */
class Actor
{
    /**
     * @ORM\Column(type="integer", name="id_actor")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $first_name;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $last_name;

    /**
     * @ORM\ManyToMany(targetEntity="WatchedMovie", mappedBy="actors")
     */
    protected $watched_movies;
    
    /**
     * Default constructor, initializes collections
     */
    public function __construct()
    {
        $this->watched_movies = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;
    }
    
    public function getFirstName()
    {
        return $this->first_name;
    }
    
    public function getLastName()
    {
        return $this->last_name;
    }

    public function addWatchedMovie(WatchedMovie $watchedMovie)
    {
        if ($this->watched_movies->contains($watchedMovie)) {
            return;
        }

        $this->watched_movies->add($watchedMovie);
        $watchedMovie->addActor($this);
    }

    public function removeWatchedMovie(WatchedMovie $watchedMovie)
    {
        if (!$this->watched_movies->contains($watchedMovie)) {
            return;
        }

        $this->watched_movies->removeElement($watchedMovie);
        $watchedMovie->removeActor($this);
    }
    
    public function findActor($doctrine)
    {
        return $doctrine->getRepository(Actor::class)
            ->findOneBy(
                array(
                    'first_name' => $this->getFirstName(), 
                    'last_name' => $this->getLastName()
                )
            );
    }
}
<?php
namespace AppBundle\Entity;

use AppBundle\QueryBuilder\DataFinancial;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="amortization")
 */
class Amortization
{
    /**
     * @ORM\Column(type="integer", name="id_amortization")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Company", cascade="persist")
     * @ORM\JoinColumn(name="id_company", referencedColumnName="id_company")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="Year", cascade="persist")
     * @ORM\JoinColumn(name="id_year", referencedColumnName="id_year")
     */
    private $year;

    /**
     * @ORM\ManyToOne(targetEntity="Quarter", cascade="persist")
     * @ORM\JoinColumn(name="id_quarter", referencedColumnName="id_quarter")
     */
    private $quarter;

    /**
     * @ORM\Column(type="integer", length=10)
     */
    private $amortization;

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getQuarter()
    {
        return $this->quarter;
    }

    /**
     * @param mixed $quarter
     */
    public function setQuarter($quarter)
    {
        $this->quarter = $quarter;
    }

    /**
     * @return mixed
     */
    public function getAmortization()
    {
        return $this->amortization;
    }

    /**
     * @param mixed $amortization
     */
    public function setAmortization($amortization)
    {
        $this->amortization = $amortization;
    }

    public function getValue()
    {
        return $this->amortization;
    }

    /**
     * @param mixed $amortization
     */
    public function setValue($amortization)
    {
        $this->amortization = $amortization;
    }

    public function findOrCreateFinancialResult($doctrine)
    {
        $netProfit = $this->getFromBase($doctrine);

        if (is_null($netProfit)) {
            $netProfit = new Amortization();
        }

        return $netProfit;
    }

    public function getFromBase($doctrine)
    {
        $repository = $doctrine
            ->getRepository(Amortization::class);

        $query = DataFinancial::createQuery($repository, $this);

        return $query->setMaxResults(1)->getOneOrNullResult();
    }




}
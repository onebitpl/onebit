<?php
namespace AppBundle\Entity;

use AppBundle\QueryBuilder\DataFinancial;
use Doctrine\ORM\Mapping as ORM;

/**
 * Zysk na akcje
 *
 * @ORM\Entity
 * @ORM\Table(name="profit_per_share")
 */
class ProfitPerShare
{
    /**
     * @ORM\Column(type="integer", name="id_profit_per_share")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Company", cascade="persist")
     * @ORM\JoinColumn(name="id_company", referencedColumnName="id_company")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="Year", cascade="persist")
     * @ORM\JoinColumn(name="id_year", referencedColumnName="id_year")
     */
    private $year;

    /**
     * @ORM\ManyToOne(targetEntity="Quarter", cascade="persist")
     * @ORM\JoinColumn(name="id_quarter", referencedColumnName="id_quarter")
     */
    private $quarter;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private $profit_per_share;

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getQuarter()
    {
        return $this->quarter;
    }

    /**
     * @param mixed $quarter
     */
    public function setQuarter($quarter)
    {
        $this->quarter = $quarter;
    }

    /**
     * @return mixed
     */
    public function getProfitPerShare()
    {
        return $this->profit_per_share;
    }

    /**
     * @param mixed $profit_per_share
     */
    public function setProfitPerShare($profit_per_share)
    {
        $this->profit_per_share = $profit_per_share;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->profit_per_share;
    }

    /**
     * @param mixed $profit_per_share
     */
    public function setValue($profit_per_share)
    {
        $this->profit_per_share = str_replace(',', '.',$profit_per_share);
    }

    public function findOrCreateFinancialResult($doctrine)
    {
        $netProfit = $this->getFromBase($doctrine);

        if (is_null($netProfit)) {
            $netProfit = new ProfitPerShare();
        }

        return $netProfit;
    }

    public function getFromBase($doctrine)
    {
        $repository = $doctrine
            ->getRepository(ProfitPerShare::class);

        $query = DataFinancial::createQuery($repository, $this);

        return $query->setMaxResults(1)->getOneOrNullResult();
    }
}
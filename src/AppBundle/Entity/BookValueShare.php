<?php
namespace AppBundle\Entity;

use AppBundle\QueryBuilder\DataFinancial;
use Doctrine\ORM\Mapping as ORM;

/**
 * wartość księgowa na akcje
 *
 * @ORM\Entity
 * @ORM\Table(name="Book_value_share")
 */
class BookValueShare
{
    /**
     * @ORM\Column(type="integer", name="id_book_value_share")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Company", cascade="persist")
     * @ORM\JoinColumn(name="id_company", referencedColumnName="id_company")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="Year", cascade="persist")
     * @ORM\JoinColumn(name="id_year", referencedColumnName="id_year")
     */
    private $year;

    /**
     * @ORM\ManyToOne(targetEntity="Quarter", cascade="persist")
     * @ORM\JoinColumn(name="id_quarter", referencedColumnName="id_quarter")
     */
    private $quarter;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private $book_value_share;

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getQuarter()
    {
        return $this->quarter;
    }

    /**
     * @param mixed $quarter
     */
    public function setQuarter($quarter)
    {
        $this->quarter = $quarter;
    }

    /**
     * @return mixed
     */
    public function getBookValueShare()
    {
        return $this->book_value_share;
    }

    /**
     * @param mixed $book_value_share
     */
    public function setBookValueShare($book_value_share)
    {
        $this->book_value_share = $book_value_share;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->book_value_share;
    }

    /**
     * @param mixed $book_value_share
     */
    public function setValue($book_value_share)
    {
        $this->book_value_share = str_replace(',','.',$book_value_share);
    }

    public function findOrCreateFinancialResult($doctrine)
    {
        $netProfit = $this->getFromBase($doctrine);

        if (is_null($netProfit)) {
            $netProfit = new BookValueShare();
        }

        return $netProfit;
    }

    public function getFromBase($doctrine)
    {
        $repository = $doctrine
            ->getRepository(BookValueShare::class);

        $query = DataFinancial::createQuery($repository, $this);

        return $query->setMaxResults(1)->getOneOrNullResult();
    }
}
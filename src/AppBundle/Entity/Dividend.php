<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="dividend")
 */
class Dividend
{
    /**
     * @ORM\Column(type="integer", name="id_dividend")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Company", cascade="persist")
     * @ORM\JoinColumn(name="id_company", referencedColumnName="id_company")
     */
    private $company;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private $paycheck;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_payment;

    /**
     * Default constructor, initializes collections
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return mixed
     */
    public function getPaycheck()
    {
        return $this->paycheck;
    }

    /**
     * @param mixed
     */
    public function getDatePayment()
    {
        return $this->date_payment;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @param mixed $paycheck
     */
    public function setPaycheck($paycheck)
    {
        $this->paycheck = $paycheck;
    }

    /**
     * @param mixed $datePayment
     */
    public function setDatePayment($datePayment)
    {
        $this->date_payment = $datePayment;
    }

    public function findOrCreateDividend($doctrine)
    {
        $dyvidend = $this->getFromBase($doctrine);

        if (is_null($dyvidend)) {
            $dyvidend = new Dividend();
        }

        return $dyvidend;
    }

    public function getFromBase($doctrine)
    {
        $repository = $doctrine
            ->getRepository(Dividend::class);

// createQueryBuilder() automatically selects FROM AppBundle:Product
// and aliases it to "p"

        $query = $repository->createQueryBuilder('p')
            ->where('p.date_payment = :date_payment')
            ->andwhere(' p.company = :company')
            ->setParameter('date_payment', $this->getDatePayment())
            ->setParameter('company', $this->getCompany()->getId())
            ->getQuery();

      //  $products = $query->getResult();
// to get just one result:
        return $query->setMaxResults(1)->getOneOrNullResult();
    }

}

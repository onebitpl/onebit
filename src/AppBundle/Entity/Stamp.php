<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="stamp")
 */
class Stamp
{
    /**
     * @ORM\Column(type="integer", name="id_stamp")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $id_catalogue_fisher;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $quality;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $price_sell;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $price_buy;

    /**
     * @ORM\Column(type="integer")
     */
    private $count;

    /**
     * Default constructor, initializes collections
     */
    public function __construct()
    {
    }
    
}
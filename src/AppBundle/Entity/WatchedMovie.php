<?php
// php bin/console doctrine:schema:update --force - generuje bazę danych
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Actor;
use AppBundle\Entity\Directory;

/**
 * @ORM\Entity
 * @ORM\Table(name="watched_movie")
 */
class WatchedMovie
{
    /**
     * @ORM\Column(type="integer", name="id_watched_movie")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $original_title;
    
    /**
     * @ORM\ManyToMany(targetEntity="Actor", inversedBy="watched_movies", cascade="persist")
     * @ORM\JoinTable(
     *  name="watched_movie_actor",
     *  joinColumns={
     *      @ORM\JoinColumn(name="id_watched_movie", referencedColumnName="id_watched_movie")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="id_actor", referencedColumnName="id_actor")
     *  }
     * )
     */
    private $actors;
    
    /**
     * @ORM\ManyToMany(targetEntity="Directory", inversedBy="watched_movies", cascade="persist")
     * @ORM\JoinTable(
     *  name="watched_movie_directory",
     *  joinColumns={
     *      @ORM\JoinColumn(name="id_watched_movie", referencedColumnName="id_watched_movie")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="id_directory", referencedColumnName="id_directory")
     *  }
     * )
     */
    private $directors;
    
    /**
     * @ORM\ManyToMany(targetEntity="Country", inversedBy="watched_movies")
     * @ORM\JoinTable(
     *  name="watched_movie_country",
     *  joinColumns={
     *      @ORM\JoinColumn(name="id_watched_movie", referencedColumnName="id_watched_movie")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="id_country", referencedColumnName="id_country")
     *  }
     * )
     */
    private $countries;
    
    /**
     * @ORM\ManyToMany(targetEntity="Genre", inversedBy="watched_movies")
     * @ORM\JoinTable(
     *  name="watched_movie_genre",
     *  joinColumns={
     *      @ORM\JoinColumn(name="id_watched_movie", referencedColumnName="id_watched_movie")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="id_genre", referencedColumnName="id_genre")
     *  }
     * )
     */
    private $genries;
    
    /**
     * @ORM\ManyToOne(targetEntity="Rating", cascade="persist")
     * @ORM\JoinColumn(name="id_rating", referencedColumnName="id_rating")
     */
    private $rating;
    
    /**
     * @ORM\ManyToOne(targetEntity="Year", cascade="persist")
     * @ORM\JoinColumn(name="id_year", referencedColumnName="id_year")
     */
    private $year;
    
    /**
     * @ORM\ManyToOne(targetEntity="MyCritique")
     * @ORM\JoinColumn(name="id_my_critique", referencedColumnName="id_my_critique")
     */
    private $my_critique;
    
    public function __construct() {
        $this->actors = new \Doctrine\Common\Collections\ArrayCollection();
        $this->directors = new \Doctrine\Common\Collections\ArrayCollection();  
        $this->countries = new \Doctrine\Common\Collections\ArrayCollection();
        $this->genries = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title) 
    {
        $this->title = $title;
    }
    public function getOriginalTitle() 
    {
        return $this->original_title;
    }
    
    public function setOriginalTitle($title) 
    {
        $this->title = $title;
    }
    
    public function getRating() 
    {
        return $this->rating;
    }
    
    public function setRating($rating) 
    {
        $this->rating = $rating;
    }
    
    public function getYear() 
    {
        return $this->year;
    }
    
    public function setYear($year) 
    {
        $this->year = $year;
    }
    
    public function getActors()
    {
        return $this->actors;
    }
    
    public function setActors($actors)
    {
        $this->actors = $actors;
    }
    
    public function getDirectors()
    {
        return $this->directors;
    }
    
    public function setDirectors($directors)
    {
        $this->directors = $directors;
    }
    
    public function getCountries()
    {
        return $this->countries;
    }
    
    public function setCountries($countries)
    {
        $this->countries = $countries;
    }
    
    public function getGenries()
    {
        return $this->genries;
    }
    
    public function setGenries($genries)
    {
        $this->genries = $genries;
    }
    
    
    public function clearActors()
    {
        $this->actors = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function addActor(Actor $actor)
    {
        if ($this->actors->contains($actor)) {
            return;
        }

        $this->actors->add($actor);
        $actor->addWatchedMovie($this);
    }
    
    public function removeActor(Actor $actor)
    {
        if (!$this->actors->contains($actor)) {
            return;
        }

        $this->actors->removeElement($actor);
        $actor->removeWatchedMovie($this);
    }
    
    public function clearDirectors()
    {
        $this->directors = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function addDirectory(Directory $directory)
    {
        if ($this->directors->contains($directory)) {
            return;
        }
        $this->directors->add($directory);
        $directory->addWatchedMovie($this);
    }
    
    public function removeDirectory(Directory $directory)
    {
        if (!$this->directors->contains($directory)) {
            return;
        }
        $this->directors->removeElement($directory);
        $directory->removeWatchedMovie($this);
    }
    
    public function addCountry(Country $country)
    {
        if ($this->countries->contains($country)) {
            return;
        }
        $this->countries->add($country);
        $country->addWatchedMovie($this);
    }
    
    public function removeCountry(Country $country)
    {
        if (!$this->countries->contains($country)) {
            return;
        }
        $this->countries->remove($country);
        $country->removewatchedMovie($this);
    }
    
    public function addGenre(Genre $genre)
    {
        if ($this->genries->contains($genre)) {
            return;
        }
        $this->genries->add($genre);
        $genre->addWatchedMovie($this);
    }
    
    public function removeGenre(Genre $genre)
    {
        if (!$this->genries->contains($genre)) {
            return;
        }
        $this->genries->remove($genre);
        $genre->removewatchedMovie($this);
    }
    
    public function addEmptyActor()
    {
        $actor = new Actor();
        $actor->setFirstName('');
        $actor->setLastName('');

        $this->getActors()->add($actor);
    }
    
    public function addEmptyDirectory()
    {
        $directory = new Directory();
        $directory->setFirstName('');
        $directory->setLastName('');

        $this->getDirectors()->add($directory);
    }
    
    public function setUniqueActors($doctrine)
    {
        $actors = $this->getActors();

        $this->clearActors();

        foreach ($actors as $actor) {
            $actor->removeWatchedMovie($this);
        }

        foreach ($actors as $actor) {

         //   if (is_null($actor->getId())) {
                $actorToAdd = $actor->findActor($doctrine);
         //   } else {
         //       $actorToAdd = $actor;
         //   }

            if (is_null($actorToAdd)) {
                $actorToAdd = new Actor();
                $actorToAdd->setFirstName($actor->getFirstName());
                $actorToAdd->setLastName($actor->getLastName());
            }

            $this->addActor($actorToAdd);
        }
    }
    
    public function setUniqueDirectors($doctrine)
    {
        $directors = $this->getDirectors();

        $this->clearDirectors();

        foreach ($directors as $directory) {

         //   if (is_null($directory->getId())) {
                $directoryToAdd = $directory->findDirectory($doctrine);
         //   } else {
         //       $directoryToAdd = $directory;
         //   }

            if (is_null($directoryToAdd)) {
                $directoryToAdd = new Directory();
                $directoryToAdd->setFirstName($directory->getFirstName());
                $directoryToAdd->setLastName($directory->getLastName());
            }

            $this->addDirectory($directoryToAdd);
        }
    }

    public function setUniqueRating($doctrine)
    {
        $rating = $doctrine->getRepository(Rating::class)->find($this->getRating()->getRating());
        $this->setRating($rating);  
    }
    
    public function setUniqueYear($doctrine)
    {
        $year = $doctrine->getRepository(Year::class)->find($this->getYear()->getYear());
        $this->setYear($year);
    }
    
    
}


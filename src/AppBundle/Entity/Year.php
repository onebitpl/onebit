<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="year")
 */
class Year
{
    /**
     * @ORM\Column(type="integer", name="id_year")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", length=4)
     */
    private $year;

    public function getYear() 
    {
        return $this->year;
    }
    public function setYear($year) 
    {
        return $this->year = $year;
    }
    public function setId($id) 
    {
        $this->id = $id;
    }
    public function getId() 
    {
        return $this->id;
    }
    
    
    
}
<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="country")
 */
class Country
{
    /**
     * @ORM\Column(type="integer", name="id_country")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $country;
    
    /**
     * @ORM\ManyToMany(targetEntity="WatchedMovie", mappedBy="countries")
     */
    protected $watched_movies;
    
     public function setCountry($country) 
    {
        $this->country = $country;
    }
    
    public function getCountry() 
    {
        return $this->country;
    }
    
    public function addWatchedMovie(WatchedMovie $watchedMovie)
    {
        if ($this->watched_movies->contains($watchedMovie)) {
            return;
        }
        
        $this->watched_movies->add($watchedMovie);
        $watchedMovie->addCountry($this);
    }

    public function removeWatchedMovie(WatchedMovie $watchedMovie)
    {
        if (!$this->watched_movies->contains($watchedMovie)) {
            return;
        }
        
        $this->watched_movies->remove($watchedMovie);
        $watchedMovie->removeCountry($this);
    }
    
    

}
<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="type_data")
 */
class TypeData
{
    /**
     * @ORM\Column(type="integer", name="id_type_data")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AvailableBuilder", cascade="persist")
     * @ORM\JoinColumn(name="id_available_builder", referencedColumnName="id_available_builder")
     */
    private $available_builder;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $how_often_download;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $type_data;

    /**
     * Default constructor, initializes collections
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTypeData()
    {
        return $this->type_data;
    }

    /**
     * @return mixed
     */
    public function getAvailableBuilder()
    {
        return $this->available_builder;
    }

    /**
     * @return mixed
     */
    public function getHowOftenDownload()
    {
        return $this->how_often_download;
    }

    /**
     * @param mixed $type_data
     */
    public function setTypeData($type_data): void
    {
        $this->type_data = $type_data;
    }

    /**
     * @param mixed $available_builder
     */
    public function setAvailableBuilder($available_builder): void
    {
        $this->available_builder = $available_builder;
    }

    /**
     * @param mixed $how_often_download
     */
    public function setHowOftenDownload($how_often_download): void
    {
        $this->how_often_download = $how_often_download;
    }
}
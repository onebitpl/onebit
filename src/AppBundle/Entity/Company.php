<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="company")
 */
class Company
{
    /**
     * @ORM\Column(type="integer", name="id_company")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name_company;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_gpw;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_consolidation;


    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private $exchange;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     */
    private $attractive;

    /**
     * @ORM\Column(type="boolean", options={"default":0})
     */
    private $meets_requirement;

    /**
     * @return mixed
     */


    /**
     * Default constructor, initializes collections
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIsGpw()
    {
        return $this->is_gpw;
    }

    /**
     * @return mixed
     */
    public function getNameCompany()
    {
        return $this->name_company;
    }

    /**
     * @return mixed
     */
    public function getExchange()
    {
        return $this->exchange;
    }

    /**
     * @return mixed
     */
    public function getIsConsolidation()
    {
        return $this->is_consolidation;
    }

    /**
     * @param mixed $is_consolidation
     */
    public function setIsConsolidation($is_consolidation): void
    {
        $this->is_consolidation = $is_consolidation;
    }

    /**
     * @param mixed $name_company
     */
    public function setNameCompany($name_company)
    {
        $this->name_company = $name_company;
    }

    /**
     * @param mixed $is_gpw
     */
    public function setIsGpw($is_gpw): void
    {
        $this->is_gpw = $is_gpw;
    }

    /**
     * @param mixed $exchange
     */
    public function setExchange($exchange)
    {
        $this->exchange = $exchange;
    }

    public function getAttractive()
    {
        return $this->attractive;
    }

    /**
     * @param mixed $attractive
     */
    public function setAttractive($attractive): void
    {
        $this->attractive = $attractive;
    }

    /**
     * @return mixed
     */
    public function getMeetsRequirement()
    {
        return $this->meets_requirement;
    }

    /**
     * @param mixed $meets_requirement
     */
    public function setMeetsRequirement($meets_requirement): void
    {
        $this->meets_requirement = $meets_requirement;
    }

    public function findOrCreateCompany($doctrine)
    {
        $company = $this->getFromBase($doctrine);

        if (is_null($company)) {
            $company = new Company();
            $company->setIsConsolidation(0);
            $company->setAttractive(0);
            $company->setMeetsRequirement(0);
        }

        return $company;
    }

    public function getFromBase($doctrine)
    {
        return $doctrine->getRepository(Company::class)
            ->findOneBy(
                array(
                    'name_company' => $this->getNameCompany()
                )
            );
    }


}

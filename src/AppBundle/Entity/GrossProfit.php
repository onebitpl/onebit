<?php
namespace AppBundle\Entity;

use AppBundle\QueryBuilder\DataFinancial;
use Doctrine\ORM\Mapping as ORM;

/**
 * zysk brutto
 *
 * @ORM\Entity
 * @ORM\Table(name="gross_profit")
 */
class GrossProfit
{
    /**
     * @ORM\Column(type="integer", name="id_gross_profit")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Company", cascade="persist")
     * @ORM\JoinColumn(name="id_company", referencedColumnName="id_company")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="Year", cascade="persist")
     * @ORM\JoinColumn(name="id_year", referencedColumnName="id_year")
     */
    private $year;

    /**
     * @ORM\ManyToOne(targetEntity="Quarter", cascade="persist")
     * @ORM\JoinColumn(name="id_quarter", referencedColumnName="id_quarter")
     */
    private $quarter;

    /**
     * @ORM\Column(type="integer", length=10)
     */
    private $gross_profit;

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getQuarter()
    {
        return $this->quarter;
    }

    /**
     * @param mixed $quarter
     */
    public function setQuarter($quarter)
    {
        $this->quarter = $quarter;
    }

    /**
     * @return mixed
     */
    public function getGrossProfit()
    {
        return $this->gross_profit;
    }

    /**
     * @param mixed $gross_profit
     */
    public function setGrossProfit($gross_profit)
    {
        $this->gross_profit = $gross_profit;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->gross_profit;
    }

    /**
     * @param mixed $gross_profit
     */
    public function setValue($gross_profit)
    {
        $this->gross_profit = $gross_profit;
    }

    public function findOrCreateFinancialResult($doctrine)
    {
        $netProfit = $this->getFromBase($doctrine);

        if (is_null($netProfit)) {
            $netProfit = new GrossProfit();
        }

        return $netProfit;
    }

    public function getFromBase($doctrine)
    {
        $repository = $doctrine
            ->getRepository(GrossProfit::class);

        $query = DataFinancial::createQuery($repository, $this);

        return $query->setMaxResults(1)->getOneOrNullResult();
    }
    
}
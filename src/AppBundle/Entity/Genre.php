<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="genre")
 */
class Genre
{
    /**
     * @ORM\Column(type="integer", name="id_genre")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $genre;

    /**
     * @ORM\ManyToMany(targetEntity="WatchedMovie", mappedBy="genries")
     */
    protected $watched_movies;
    
     public function setGenre($genre) 
    {
        $this->genre = $genre;
    }
    
    public function getGenre() 
    {
        return $this->genre;
    }
    
    public function addWatchedMovie(WatchedMovie $watchedMovie)
    {
        if ($this->watched_movies->contains($watchedMovie)) {
            return;
        }
        
        $this->watched_movies->add($watchedMovie);
        $watchedMovie->addGenre($this);
    }

    public function removeWatchedMovie(WatchedMovie $watchedMovie)
    {
        if (!$this->watched_movies->contains($watchedMovie)) {
            return;
        }
        
        $this->watched_movies->remove($watchedMovie);
        $watchedMovie->removeGenre($this);
    }
}
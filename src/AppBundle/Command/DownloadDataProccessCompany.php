<?php
namespace AppBundle\Command;

use AppBundle\Menagers\MenagerCompanyDownload;
use AppBundle\QueryBuilder\FirstPriorityDownload;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class DownloadDataProccessCompany extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:download-data-all')

            // the short description shown while running "php bin/console list"
            ->setDescription('download data company.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $process = new Process('php ./bin/console app:download-data');
        $process->run();

        $priorityDownloads = count(FirstPriorityDownload::getAll($this->getContainer()->get('doctrine')));

        // $priorityDownloads = 2;
        while ($priorityDownloads > 0) {
            $process = new Process('php ./bin/console app:download-data');
            $process->run();


            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }

            echo $process->getOutput();

            $priorityDownloads--;
            $output->writeln('pozostało rekordów: '.$priorityDownloads);
        }

    }
}
<?php
namespace AppBundle\Command;

use AppBundle\Menagers\MenagerCompanyDownload;
use AppBundle\QueryBuilder\FirstPriorityDownload;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class DownloadDataCompany extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:download-data')

            // the short description shown while running "php bin/console list"
            ->setDescription('download data company.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $menager = new MenagerCompanyDownload();
        $menager->run($this->getContainer()->get('doctrine'));
    }
}
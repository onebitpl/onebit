<?php

namespace AppBundle\Menagers\GenerateDatabase;

use AppBundle\Entity\PriorityDownload;
use AppBundle\Entity\TypeData;
use AppBundle\ObjectProvider\DataTimeProvider;
use AppBundle\ObjectProvider\PriorityDownloadProvider;
use AppBundle\QueryBuilder\CompanyPriority;

class Priority
{
    public static function addPriorityDownload($doctrine)
    {
        $entityManager = $doctrine->getManager();
        $typeDatas = $doctrine
            ->getRepository(TypeData::class)
            ->findBy(array());


        $counter = 0;
        foreach ($typeDatas as $typeData) {
            $companies = CompanyPriority::noDataFinancial($doctrine, $typeData->getId());
            foreach ($companies as $company) {
                $priorityDownload = PriorityDownloadProvider::get($company, $typeData);
                $entityManager->persist($priorityDownload);
                $counter++;
            }
        }

        self::flush($counter, $entityManager);

        var_dump('wygenerowano ' . $counter . ' wpisów priorytetowych');
    }

    public static function increasePriority($doctrine)
    {
        $entityManager = $doctrine->getManager();

        $priorityDownloads = $doctrine
            ->getRepository(PriorityDownload::class)
            ->findBy(array());

        $counter = 0;
        foreach ($priorityDownloads as $priorityDownload) {
            $expireDate = DataTimeProvider::get('-' . $priorityDownload->getTypeData()->getHowOftenDownload());

            if ($priorityDownload->getLastDownload() < $expireDate) {
                $priorityDownload->setPriority($priorityDownload->getPriority() + 1);
                $entityManager->persist($priorityDownload);
                $counter++;
            }
        }

        self::flush($counter, $entityManager);
    }

    private static function flush($counter, $entityManager)
    {
        if ($counter) {
            $entityManager->flush();
        }
    }
}
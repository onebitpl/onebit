<?php

namespace AppBundle\Menagers\Control;

class InfoControl
{
    private $nameCompany = null;
    private $idDataType = null;
    private $company = null;
    private $nameBuilder = null;
    private $countPagination = 0;
    private $allIsUpdated = false;
    private $priorityDownloads = null;


    public function setInfo($priorityDownloads)
    {
        $this->priorityDownloads = $priorityDownloads;
        $this->idDataType = $priorityDownloads->getTypeData()->getId();
        $this->company = $priorityDownloads->getCompany();
        $this->nameCompany = $this->company->getNameCompany();
        $this->nameBuilder = $priorityDownloads->getTypeData()->getAvailableBuilder()->getBuilderName();
    }

    /**
     * @return null
     */
    public function getNameCompany()
    {
        return $this->nameCompany;
    }

    /**
     * @param null $nameCompany
     */
    public function setNameCompany($nameCompany): void
    {
        $this->nameCompany = $nameCompany;
    }

    /**
     * @return null
     */
    public function getIdDataType()
    {
        return $this->idDataType;
    }

    /**
     * @param null $dataType
     */
    public function setIdDataType($idDataType): void
    {
        $this->idDataType = $idDataType;
    }

    /**
     * @return null
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param null $company
     */
    public function setCompany($company): void
    {
        $this->company = $company;
        $this->nameCompany = $this->company->getNameCompany();
    }

    /**
     * @return null
     */
    public function getNameBuilder()
    {
        return $this->nameBuilder;
    }

    /**
     * @param null $nameBuilder
     */
    public function setNameBuilder($nameBuilder): void
    {
        $this->nameBuilder = $nameBuilder;
    }

    /**
     * @return int
     */
    public function getCountPagination(): int
    {
        return $this->countPagination;
    }

    /**
     * @param int $countPagination
     */
    public function setCountPagination(int $countPagination): void
    {
        $this->countPagination = $countPagination;
    }

    /**
     * @return bool
     */
    public function getAllIsUpdated(): bool
    {
        return $this->allIsUpdated;
    }

    /**
     * @param bool $allIsUpdated
     */
    public function setAllIsUpdated(bool $allIsUpdated): void
    {
        $this->allIsUpdated = $allIsUpdated;
    }

    /**
     * @return null
     */
    public function getPriorityDownloads()
    {
        return $this->priorityDownloads;
    }

    /**
     * @param null $priorityDownloads
     */
    public function setPriorityDownloads($priorityDownloads): void
    {
        $this->priorityDownloads = $priorityDownloads;
    }
}
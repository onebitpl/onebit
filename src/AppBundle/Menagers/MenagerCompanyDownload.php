<?php

namespace AppBundle\Menagers;

use AppBundle\Menagers\Control\InfoControl;
use AppBundle\Menagers\GenerateDatabase\Priority;
use AppBundle\QueryBuilder\FirstPriorityDownload;



class MenagerCompanyDownload
{
    private $doctrine = null;
    private $infoControl = null;

    public function run($doctrine)
    {
        $this->doctrine = $doctrine;
        $entityManager = $this->doctrine->getManager();
        $this->infoControl = new InfoControl();

        Priority::addPriorityDownload($doctrine);
        Priority::increasePriority($doctrine);

        $result = $this->updateDataCompany();

        if ($this->infoControl->getAllIsUpdated()) {
            return;
        }

        $entityManager->persist($this->infoControl->getCompany());
        $entityManager->flush();


        $managerSave = new MenagerSaveCompanyData();
        $managerSave->save($doctrine, $result, $this->infoControl);
    }

    public function updateDataCompany()
    {
        $priorityDownloads = FirstPriorityDownload::get($this->doctrine);

        if (is_null($priorityDownloads)) {
            $this->infoControl->setAllIsUpdated(true);
            return false;
        }

        $this->infoControl->setInfo($priorityDownloads);
        var_dump($this->infoControl->getNameCompany());
        $builder = $this->infoControl->getNameBuilder();

        $dataResult = new $builder();
        $dataResult->setInfo($this->infoControl);

        return $dataResult->getResult();
    }

}
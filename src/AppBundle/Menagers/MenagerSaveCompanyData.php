<?php

namespace AppBundle\Menagers;

use AppBundle\Entity\Amortization;
use AppBundle\Entity\Assets;
use AppBundle\Entity\Company;
use AppBundle\Entity\Dividend;
use AppBundle\Entity\Ebitda;
use AppBundle\Entity\GrossProfit;
use AppBundle\Entity\NetIncome;
use AppBundle\Entity\NetProfit;
use AppBundle\Entity\EquityCapital;
use AppBundle\Entity\CountStockShare;
use AppBundle\Entity\ProfitPerShare;
use AppBundle\Entity\BookValueShare;
use AppBundle\Entity\Quarter;
use AppBundle\Entity\Year;
use AppBundle\Entity\PriorityDownload;
use AppBundle\Services\TextModifier;


class MenagerSaveCompanyData
{
    private $doctrine = null;
    private $entityManager = null;
    private $nameCompany = null;
    private $deleteCompany = false;
    private $infoControl = null;


    private $countPagination = 0;


    public function save($doctrine, $result, $infoControl)
    {
      //  return;
        $this->doctrine = $doctrine;
        $this->entityManager = $this->doctrine->getManager();

        $this->infoControl = $infoControl;
       // $this->dataType = $infoControl->getIdDataType();
       // $this->company = $infoControl->getCompany();
        $this->nameCompany = $infoControl->getNameCompany();
        $this->countPagination = $infoControl->getCountPagination();

        $this->saveDataCompany($result);

        if ($this->deleteCompany) {
            $this->deleteCompany();
        }
      //  die();

    }


    public function deleteCompany()
    {
        $company = $this->doctrine->getRepository(Company::class)
            ->findOneBy(
                ['name_company' => $this->nameCompany]
            );
        $priorityDownloads = $this->doctrine->getRepository(PriorityDownload::class)
            ->findBy(
                ['company' => $company->getId()]
            );

        $ebitdas = $this->doctrine->getRepository(Ebitda::class)
            ->findBy(
                ['company' => $company->getId()]
            );

        $dividends = $this->doctrine->getRepository(Dividend::class)
            ->findBy(
                ['company' => $company->getId()]
            );

        $netIncomes = $this->doctrine->getRepository(NetIncome::class)
            ->findBy(
                ['company' => $company->getId()]
            );

        $bookValueShares = $this->doctrine->getRepository(BookValueShare::class)
            ->findBy(
                ['company' => $company->getId()]
            );

        $assets = $this->doctrine->getRepository(Assets::class)
            ->findBy(
                ['company' => $company->getId()]
            );

        $amortizations = $this->doctrine->getRepository(Amortization::class)
            ->findBy(
                ['company' => $company->getId()]
            );

        $netProfits = $this->doctrine->getRepository(NetProfit::class)
            ->findBy(
                ['company' => $company->getId()]
            );

        $countStockShares = $this->doctrine->getRepository(CountStockShare::class)
            ->findBy(
                ['company' => $company->getId()]
            );

        $profitPerShares = $this->doctrine->getRepository(ProfitPerShare::class)
            ->findBy(
                ['company' => $company->getId()]
            );

        $equityCapitals = $this->doctrine->getRepository(EquityCapital::class)
            ->findBy(
                ['company' => $company->getId()]
            );

        $grossProfits = $this->doctrine->getRepository(GrossProfit::class)
            ->findBy(
                ['company' => $company->getId()]
            );

     //  var_dump($company->getId());

        foreach($priorityDownloads as $priorityDownload) {
            $this->entityManager->remove($priorityDownload);
        }

        foreach($ebitdas as $ebitda) {
            $this->entityManager->remove($ebitda);
        }

        foreach($dividends as $dividend) {
            $this->entityManager->remove($dividend);
        }

        foreach($netIncomes as $netIncome) {
            $this->entityManager->remove($netIncome);
        }

        foreach($bookValueShares as $bookValueShare) {
            $this->entityManager->remove($bookValueShare);
        }

        foreach($assets as $asset) {
            $this->entityManager->remove($asset);
        }

        foreach($amortizations as $amortization) {
            $this->entityManager->remove($amortization);
        }

        foreach($netProfits as $netProfit) {
            $this->entityManager->remove($netProfit);
        }

        foreach($countStockShares as $countStockShare) {
            $this->entityManager->remove($countStockShare);
        }

        foreach($profitPerShares as $profitPerShare) {
            $this->entityManager->remove($profitPerShare);
        }

        foreach($equityCapitals as $equityCapital) {
            $this->entityManager->remove($equityCapital);
        }

        foreach($grossProfits as $grossProfit) {
            $this->entityManager->remove($grossProfit);
        }

        $this->entityManager->remove($company);
        $this->entityManager->flush();
    }

    public function saveDataCompany($resultToSave)
    {
        var_dump(count($resultToSave) . " - danych do zapisania");
        $companiesAreActualized = [];
        $dateTime = new \DateTime('now');

        $thisIsActualize = false;
        if (empty($resultToSave)) {
            //var_dump($resultToSave);
            $this->deleteCompany = false;
            $query = $this->doctrine->getRepository(PriorityDownload::class);

            $priorityDownload = $query
                ->createQueryBuilder('p')
                ->leftJoin('p.company', 'company')
                ->where('company.name_company = :nameCompany')
                ->andwhere('p.type_data = :typeData')
                ->setParameter('nameCompany', $this->nameCompany)
                ->setParameter('typeData', $this->infoControl->getIdDataType())
                ->getQuery()
                ->setMaxResults(1)
                ->getOneOrNullResult();


            $priorityDownload->setPriority(0);
            $priorityDownload->setLastDownload($dateTime);
            $this->entityManager->persist($priorityDownload);
        }


      //  var_dump($this->nameCompany);

      //  echo '<br>';
     //   echo '<br>';
     //   echo '<br>';
      //  echo '<br>';

        if ($this->infoControl->getIdDataType() == 4) {
            $this->deleteCompany = true;
            foreach ($resultToSave as $result) {
                if ($this->nameCompany == $result->getNameCompany()) {
                    $this->deleteCompany = false;
                }
            }
        }

        foreach ($resultToSave as $result) {
            if ($result instanceof Company) {
           //     echo '<br>';
            //    var_dump($result->getNameCompany());

                $this->saveCompany($result);
                /**
                 * @var PriorityDownload $priorityDownload
                 */
                $query = $this->doctrine->getRepository(PriorityDownload::class);


                $priorityDownload = $query
                    ->createQueryBuilder('p')
                    ->leftJoin('p.company', 'company')
                    ->where('company.name_company = :nameCompany')
                    ->andwhere('p.type_data = :typeData')
                    ->setParameter('nameCompany', $result->getNameCompany())
                    ->setParameter('typeData', $this->infoControl->getIdDataType())
                    ->getQuery()
                    ->setMaxResults(1)
                    ->getOneOrNullResult();

                if (is_null($priorityDownload)) continue;

                $priorityDownload->setPriority(0);
                $priorityDownload->setLastDownload($dateTime);
                $this->entityManager->persist($priorityDownload);

                if ($result->getNameCompany() == $this->nameCompany) {
                    $thisIsActualize = true;
                }
            }

            if ($result instanceof Dividend) {

                $this->saveDividends($result, $this->nameCompany);
                /**
                 * @var PriorityDownload $priorityDownload
                 */
                $query = $this->doctrine->getRepository(PriorityDownload::class);

                $priorityDownload = $query
                    ->createQueryBuilder('p')
                    ->leftJoin('p.company', 'company')
                    ->where('company.name_company = :nameCompany')
                    ->andwhere('p.type_data = :typeData')
                    ->setParameter('nameCompany', $this->nameCompany)
                    ->setParameter('typeData', $this->infoControl->getIdDataType())
                    ->getQuery()
                    ->setMaxResults(1)
                    ->getOneOrNullResult();

                $priorityDownload->setPriority(0);
                $priorityDownload->setLastDownload($dateTime);
                $this->entityManager->persist($priorityDownload);

                $thisIsActualize = true;
            }

            if (!($result instanceof Dividend) && !($result instanceof Company)) {

                $this->saveFinancialsResult($result, $this->nameCompany);
                /**
                 * @var PriorityDownload $priorityDownload
                 */
                $query = $this->doctrine->getRepository(PriorityDownload::class);

                $dataType = $this->infoControl->getIdDataType();
                if ($result instanceof Amortization) {
                    $dataType = 1;
                }
                if ($result instanceof Assets) {
                    $dataType = 2;
                }
                if ($result instanceof BookValueShare) {
                    $dataType = 3;
                }
                if ($result instanceof CountStockShare) {
                    $dataType = 5;
                }
                if ($result instanceof Ebitda) {
                    $dataType = 7;
                }
                if ($result instanceof EquityCapital) {
                    $dataType = 8;
                }
                if ($result instanceof GrossProfit) {
                    $dataType = 9;
                }
                if ($result instanceof NetIncome) {
                    $dataType = 10;
                }
                if ($result instanceof NetProfit) {
                    $dataType = 11;
                }
                if ($result instanceof ProfitPerShare) {
                    $dataType = 12;
                }

                $priorityDownload = $query
                    ->createQueryBuilder('p')
                    ->leftJoin('p.company', 'company')
                    ->where('company.name_company = :nameCompany')
                    ->andwhere('p.type_data = :typeData')
                    ->setParameter('nameCompany', $this->nameCompany)
                    ->setParameter('typeData', $dataType)
                    ->getQuery()
                    ->setMaxResults(1)
                    ->getOneOrNullResult();


                if ($this->countPagination == 0) {
                    $priorityDownload->setPriority(0);
                    $priorityDownload->setLastDownload($dateTime);
                    $priorityDownload->setLastPagination($this->countPagination);
                } else {
                    $priorityDownload->setLastPagination($this->countPagination);
                }
                $this->entityManager->persist($priorityDownload);

                if ($dataType == $this->infoControl->getIdDataType()) {
                    $thisIsActualize = true;
                }

            }
        }

        if (!$thisIsActualize && ($this->infoControl->getIdDataType() != 4)) {
            $query = $this->doctrine->getRepository(PriorityDownload::class);

            $priorityDownload = $query
                ->createQueryBuilder('p')
                ->leftJoin('p.company', 'company')
                ->where('company.name_company = :nameCompany')
                ->andwhere('p.type_data = :typeData')
                ->setParameter('nameCompany', $this->nameCompany)
                ->setParameter('typeData', $this->infoControl->getIdDataType())
                ->getQuery()
                ->setMaxResults(1)
                ->getOneOrNullResult();


            $priorityDownload->setPriority(0);
            $priorityDownload->setLastDownload($dateTime);
            $this->entityManager->persist($priorityDownload);
        }

        $this->entityManager->flush();
     //   $object = null;

     //   return $object;
    }

    private function saveCompany($company)
    {
        $companyToChange = $company->findOrCreateCompany($this->doctrine);
        $companyToChange->setNameCompany($company->getNameCompany());
        $companyToChange->setExchange($company->getExchange());
        $companyToChange->setIsgpw($company->getIsGpw());


        $this->entityManager->persist($companyToChange);
    }

    private function saveDividends(Dividend $dividend, $nameCompany)
    {
        $entityManager = $this->doctrine->getManager();

        $company = $this->getCompany($nameCompany);

        $dividend->setCompany($company);

        $dividendsToChange = $dividend->findOrCreateDividend($this->doctrine);
        $dividendsToChange->setCompany($company);
        $dateImmutable = new \DateTime($dividend->getDatePayment());
        $dividendsToChange->setDatePayment($dateImmutable);
        $dividendsToChange->setPaycheck($dividend->getPaycheck());

        $entityManager->persist($dividendsToChange);
    }

    private function saveFinancialsResult($financialResult, $nameCompany)
    {
        $entityManager = $this->doctrine->getManager();

        $company = $this->getCompany($nameCompany);

        $quarter = $this->getQuarter($financialResult->getQuarter()->getDescription());
        $year = $this->getYear($financialResult->getYear()->getYear());
        $financialResult->setCompany($company);

        $financialToChange = $financialResult->findOrCreateFinancialResult($this->doctrine);

        $financialToChange->setCompany($company);
        $financialToChange->setQuarter($quarter);
        $financialToChange->setYear($year);

        $financialToChange->setValue(TextModifier::removeWhiteSpace($financialResult->getValue()));

        $entityManager->persist($financialToChange);

    }

    private function getCompany($nameCompany)
    {
        return $this->doctrine->getRepository(Company::class)
            ->findOneBy(
                array(
                    'name_company' => $nameCompany
                )
            );
    }

    private function getQuarter($description)
    {

        return $this->doctrine->getRepository(Quarter::class)
            ->findOneBy(
                array(
                    'description' => $description
                )
            );
    }

    private function getYear($year)
    {
        return $this->doctrine->getRepository(Year::class)
            ->findOneBy(
                array(
                    'year' => $year
                )
            );
    }
}
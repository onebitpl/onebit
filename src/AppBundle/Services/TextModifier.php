<?php

namespace AppBundle\Services;

class TextModifier
{
    public static function removeWhiteSpace($string)
    {
        $string = preg_replace('/\\s+/iu', '', $string);

        return $string;
    }
}
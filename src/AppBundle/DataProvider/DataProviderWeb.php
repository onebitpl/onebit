<?php

namespace AppBundle\DataProvider;

use AppBundle\DataProvider\Interfaces\DataProviderWebInterface;
use Symfony\Component\DomCrawler\Crawler;

use Exception;

class DataProviderWeb implements DataProviderWebInterface
{
    private $domDocument = null;

    public function __construct($html)
    {
        $this->domDocument = new Crawler($html);
    }

    /**
     * @param mixed $html
     */
    public function setHtml($html)
    {
        $this->domDocument = new Crawler($html);
    }

    /**
     * @param $select
     * @param null $eq pobieramy dane z konkretnego rekordu
     * @return array
     * @throws Exception
     */
    public function getValueBySelect($select, $eq = null)
    {
        if ($this->htmlDontExist()) {
            throw new Exception('brak htmla');
        }

        $data = [];
        $nodes = $this->domDocument->filter($select);

        if ( ! is_null($eq)) {
            $nodes = $nodes->eq($eq);
        }

        foreach ($nodes as $node) {
            $data[] = trim($node->nodeValue);
        }

        return $data;
    }

    /**
     * @param $select
     *
     * @return array
     * @throws Exception
     */
    public function getAttributeBySelect($select)
    {
        if ($this->htmlDontExist()) {
            throw new Exception('brak htmla');
        }

        $data = [];
        $nodes = $this->domDocument->filter($select);

        foreach ($nodes as $node) {
            $data[] = trim($node->getAttribute('href'));
        }

        return $data;
    }

    public function getCountValueBySelect($select, $eq = null)
    {
        if ($this->htmlDontExist()) {
            throw new Exception('brak htmla');
        }

        return $this->domDocument->filter($select)->count();
    }

    private function htmlDontExist()
    {
        if (is_null($this->domDocument->getNode(0))) {
            return true;
        }

        return false;
    }
}
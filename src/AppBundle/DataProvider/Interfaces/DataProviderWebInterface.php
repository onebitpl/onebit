<?php

namespace AppBundle\DataProvider\Interfaces;

interface DataProviderWebInterface
{
    public function setHtml($html);
    public function getValueBySelect($select, $eq);
    public function getCountValueBySelect($select);

}
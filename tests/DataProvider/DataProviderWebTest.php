<?php

namespace Tests\DataProvider;

use PHPUnit\Framework\TestCase;
use AppBundle\DataProvider\DataProviderWeb;

use Exception;

final class DataProviderWebTest extends TestCase
{
    const HTML = '
            <div class="test">
                <div>value</div>
            </div>
            <div class="test">
                <div>value2</div>
            <div>';

    public function testCountValueBySelect()
    {

        $dataProvider = new DataProviderWeb(self::HTML);

        $this->assertSame(2, $dataProvider->getCountValueBySelect('.test'));
    }

    public function testValueBySelect()
    {
        $dataProvider = new DataProviderWeb(self::HTML);

        $this->assertSame('value', $dataProvider->getValueBySelect('.test', 0)[0]);
        $this->assertSame('value2', $dataProvider->getValueBySelect('.test', 1)[0]);
        $this->assertSame(implode(',', ['value', 'value2']), implode(',', $dataProvider->getValueBySelect('.test')));
    }

    public function testException()
    {
        $this->expectException(Exception::class);
        $dataProvider = new DataProviderWeb('');
        $dataProvider->getValueBySelect('.test', 0);
    }

}
<?php

namespace Tests\CurlHelper;

use AppBundle\DataBuilderCompany\DataBuilderCompanyBankier;
use PHPUnit\Framework\TestCase;
use AppBundle\Curl\CurlHelper;

final class CurlHelperTest extends TestCase
{
    public function testStatusWebPages()
    {
        $curl = new CurlHelper();
        $curl->downloadWeb('https://www.bankier.pl/gielda/notowania/akcje');

        $this->assertSame(200, $curl->getStatus());
    }

}
<?php

namespace Tests\Services;

use AppBundle\Services\TextModifier;
use PHPUnit\Framework\TestCase;

final class StringTest extends TestCase
{
    const TEXT = '123 abc';

    public function testCountValueBySelect()
    {
        $this->assertSame('123abc', TextModifier::removeWhiteSpace(self::TEXT));
    }
}
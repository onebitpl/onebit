<?php

use prototype\Factory\TerrainFactory;
use prototype\Flaps\Forest\EarthForest;
use prototype\Flaps\Plains\EarthPlains;
use prototype\Flaps\Sea\EarthSea;

$earthFactory = new TerrainFactory(
    new EarthSea,
    new EarthForest,
    new EarthPlains
);

var_dump($earthFactory->getSea());
var_dump($earthFactory->getForest());
var_dump($earthFactory->getPlains());

<?php

use abstractFactory\Managers\CommsManager;
use serviceLocator\AppConfig;

$commMgr = AppConfig::getInstance()->getCommsManager();
$commMgr->make(CommsManager::APPT)->encode();

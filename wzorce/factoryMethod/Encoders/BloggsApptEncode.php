<?php

namespace factoryMethod\Encoders;

use factoryMethod\Encoders\Appt\ApptEncoder;

class BloggsApptEncode extends ApptEncoder {
    public function encode() : string
    {
        return "Dane spotkania zakodowane w formacie BloggsCall\n";
    }
}
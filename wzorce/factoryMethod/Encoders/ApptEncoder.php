<?php

namespace factoryMethod\Encoders;

abstract class ApptEncoder
{
    abstract public function encode() : string;
}
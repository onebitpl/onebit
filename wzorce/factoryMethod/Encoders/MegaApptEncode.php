<?php

namespace factoryMethod\Encoders;

use factoryMethod\Encoders\Appt\ApptEncoder;

class MegaApptEncode extends ApptEncoder {
    public function encode() : string
    {
        return "Dane spotkania zakodowane w formacie MegaCall\n";
    }
}
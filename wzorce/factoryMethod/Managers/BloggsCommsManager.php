<?php

namespace FactoryMethod\Managers;

use factoryMethod\Encoders\Appt\ApptEncoder;
use factoryMethod\Encoders\Appt\BloggsApptEncode;

class BloggsCommsManager extends CommsManager
{

    public function getHeaderText(): string
    {
        return "Nagłówek bloggsCall\n";
    }

    public function getApptEncoder(): ApptEncoder
    {
        return new BloggsApptEncode();
    }

    public function getFooterText(): string
    {
        return "Stopka bloggsCall\n";
    }
}
<?php

namespace FactoryMethod\Managers;

use factoryMethod\Encoders\Appt\ApptEncoder;
use factoryMethod\Encoders\Appt\MegaApptEncode;


class MegaCommsManager extends CommsManager
{

    public function getHeaderText(): string
    {
        return "Nagłówek MegaCall\n";
    }

    public function getApptEncoder(): ApptEncoder
    {
        return new MegaApptEncode();
    }

    public function getFooterText(): string
    {
        return "Stopka MegaCall\n";
    }
}
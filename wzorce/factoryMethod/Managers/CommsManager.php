<?php

namespace FactoryMethod\Managers;
use factoryMethod\Encoders\Appt\ApptEncoder;

abstract class CommsManager
{
    abstract public function getHeaderText() : string;
    abstract public function getApptEncoder() : ApptEncoder;
    abstract public function getFooterText() : string;
}
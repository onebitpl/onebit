<?php

use FactoryMethod\Managers\BloggsCommsManager;

$mgr = new BloggsCommsManager();

echo $mgr->getHeaderText();

echo $mgr->getApptEncoder()->encode();

echo $mgr->getFooterText();
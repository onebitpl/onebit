<?php

namespace abstractFactory\Managers;

use abstractFactory\Encoders\Interfaces\Encoder;

abstract class CommsManager
{
    const APPT = 1;
    const TTD = 2;
    const CONTACT = 3;

    abstract public function getHeaderText() : string;

    abstract public function make(int $flagInt) : Encoder;

    abstract public function getFooterText() : string;
}
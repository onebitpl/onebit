<?php

namespace abstractFactory\Managers;


use abstractFactory\Encoders\Appt\MegaApptEncode;
use abstractFactory\Encoders\Contact\MegaContactEncode;
use abstractFactory\Encoders\Ttd\MegaTtdEncode;
use abstractFactory\Encoders\Interfaces\Encoder;

class MegaCommsManager extends CommsManager
{

    public function getHeaderText(): string
    {
        return "Nagłówek MegaCall\n";
    }

    public function make(int $flagInt): Encoder
    {
        switch ($flagInt) {
            case self::APPT:
                return new MegaApptEncode();
            case self::CONTACT:
                return new MegaContactEncode();
            case self::TTD:
                return new MegaTtdEncode();
        }
    }


    public function getFooterText(): string
    {
        return "Stopka MegaCall\n";
    }
}
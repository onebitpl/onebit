<?php

namespace abstractFactory\Managers;


use abstractFactory\Encoders\Appt\BloggsApptEncode;
use abstractFactory\Encoders\Contact\BloggsContactEncode;
use abstractFactory\Encoders\Ttd\BloggsTtdEncode;
use abstractFactory\Encoders\Interfaces\Encoder;

class BloggsCommsManager extends CommsManager
{

    public function getHeaderText(): string
    {
        return "Nagłówek bloggsCall\n";
    }

    public function make(int $flagInt): Encoder
    {
        switch ($flagInt) {
            case self::APPT:
                return new BloggsApptEncode();
            case self::CONTACT:
                return new BloggsContactEncode();
            case self::TTD:
                return new BloggsTtdEncode();
        }
    }


    public function getFooterText(): string
    {
        return "Stopka bloggsCall\n";
    }
}
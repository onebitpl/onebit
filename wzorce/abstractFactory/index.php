<?php

use abstractFactory\Managers\BloggsCommsManager;
use abstractFactory\Managers\CommsManager;

$mgr = new BloggsCommsManager();

echo $mgr->getHeaderText();

echo $mgr->make(CommsManager::APPT)->encode();

echo $mgr->getFooterText();
<?php

namespace abstractFactory\Encoders\Contact;

use abstractFactory\Encoders\Interfaces\Encoder;

class MegaContactEncode implements Encoder {
    public function encode() : string
    {
        return "Dane kontaktu zakodowane w formacie MegaCall\n";
    }
}
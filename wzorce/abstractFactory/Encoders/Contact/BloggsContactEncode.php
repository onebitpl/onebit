<?php

namespace abstractFactory\Encoders\Contact;

use abstractFactory\Encoders\Interfaces\Encoder;

class BloggsContactEncode implements Encoder
{
    public function encode() : string
    {
        return "Dane kontaktu zakodowane w formacie BloggsCall\n";
    }
}
<?php

namespace abstractFactory\Encoders\Ttd;

use abstractFactory\Encoders\Interfaces\Encoder;

class MegaTtdEncode implements Encoder{
    public function encode() : string
    {
        return "Dane zadań zaplanowanych zakodowane w formacie MegaCall\n";
    }
}
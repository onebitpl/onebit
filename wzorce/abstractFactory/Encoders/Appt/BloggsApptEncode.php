<?php

namespace abstractFactory\Encoders\Appt;

use abstractFactory\Encoders\Interfaces\Encoder;

class BloggsApptEncode implements Encoder {
    public function encode() : string
    {
        return "Dane spotkania zakodowane w formacie BloggsCall\n";
    }
}
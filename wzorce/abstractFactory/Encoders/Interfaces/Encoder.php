<?php


namespace abstractFactory\Encoders\Interfaces;

interface Encoder
{
    public function encode(): string;
}
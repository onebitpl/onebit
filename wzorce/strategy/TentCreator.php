<?php

namespace strategy;

class TentCreator
{
    private $tentType;

    public function __construct(TentSize $tentType, $width = 3, $length = 4)
    {
        $this->tentType = $tentType;
        $this->width = $width;
        $this->length = $length;

    }

    public function createTent()
    {
       return $this->tentType->createTent($this->width, $this->length);
    }



}

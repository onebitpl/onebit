<?php
namespace strategy;

interface  TentSize
{
    public function createTent($width, $length);
}

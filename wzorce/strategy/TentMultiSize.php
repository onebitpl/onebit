<?php
namespace strategy;

class TentMultiSize implements TentSize
{
    public function createTent($width, $length)
    {
        $minAbs = 100;

        for($index = 1; $index < 10; $index++) {
            if ( abs($width - $index) < $minAbs) {
                $minAbs = abs($width - $index);
                $bestSolutionWidth = $index;
            }
        }

        $minAbs = 100;
        for($index = 1; $index < 10; $index++) {
            if ( abs($length - $index) < $minAbs) {
                $minAbs = abs($length - $index);
                $bestSolutionLength = $index;
            }
        }

        return [$bestSolutionWidth, $bestSolutionLength];
    }

}

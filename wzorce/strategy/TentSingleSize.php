<?php
namespace strategy;

class TentSingleSize implements TentSize
{
    private $width = [4,5,6];
    private $lengths = [4,6,8];


    public function createTent($width, $length)
    {
        $minAbs = 100;

        foreach($this->width as $singleSize) {
            if ( abs($width - $singleSize) < $minAbs) {
                $minAbs = abs($width - $singleSize);
                $bestSolutionWidth = $singleSize;
            }
        }

        $minAbs = 100;
        foreach($this->lengths as $singleSize) {
            if ( abs($length - $singleSize) < $minAbs) {
                $minAbs = abs($length - $singleSize);
                $bestSolutionLength = $singleSize;
            }
        }

        return [$bestSolutionWidth, $bestSolutionLength];
    }


}

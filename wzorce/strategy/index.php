<?php

use strategy\TentCreator;
use strategy\TentMultiSize;
use strategy\TentSingleSize;

$tent1 = new TentCreator(new TentMultiSize(), 2, 3);
$tent2 = new TentCreator(new TentMultiSize(), -6, 12);
$tent3 = new TentCreator(new TentMultiSize(), 15, 3);
$tent4 = new TentCreator(new TentMultiSize(), 7, 5);
$tent5 = new TentCreator(new TentSingleSize(), 8, 4);
$tent6 = new TentCreator(new TentSingleSize(), 13, 4);
$tent7 = new TentCreator(new TentSingleSize(), 8, 34);
$tent8 = new TentCreator(new TentSingleSize(), 6, 6);

var_dump($tent1->createTent());
echo '<br>';
var_dump($tent2->createTent());
echo '<br>';
var_dump($tent3->createTent());
echo '<br>';
var_dump($tent4->createTent());
echo '<br>';
var_dump($tent5->createTent());
echo '<br>';
var_dump($tent6->createTent());
echo '<br>';
var_dump($tent7->createTent());
echo '<br>';
var_dump($tent8->createTent());
echo '<br>';

// mówisz po polsku
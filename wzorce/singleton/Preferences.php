<?php

namespace singleton;

namespace Singleton;

class Preferences
{
    private $props = [];
    private static $instance;

    private function __comstruct()
    {
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new Preferences();
        }

        return self::$instance;
    }

    public function setProperty(string $key, string $value)
    {
        $this->props[$key] = $value;
    }

    public function getProperty(string $key)
    {
        return $this->props[$key];
    }

}

function randMax(max) {
    return Math.trunc(1E9 *Math.random()) % max;
}

var reel = {
    symbols: [
        "X","Y","Z","W","$","*","<","@"
    ],
    spin() {
        if (this.position == null) {
            this.position = randMax(
                this.symbols.length - 1
            );
        }

        this.position = ( this.position + 100 + randMax(100)) % this.symbols.length;
    },

    display() {
        if (this.position == null) {
            this.position = randMax(
                this.symbols.length - 1
            );
        }

        return this.symbols[this.position];
    }
}

var slotMachine = {
    reels: [
        Object.create(reel),
        Object.create(reel),
        Object.create(reel)
    ],
    spin() {
        this.reels.forEach( function spinReel(reel) {
            reel.spin();
        });
    },
    display() {
        let viewReel = ' ';
        let viewTopReel = ' ';
        let viewBottonReel = ' ';

        this.reels.forEach( function spinReel( singleReel, index) {
            let singleTopReel = Object.create(singleReel);
            let singleBottomReel = Object.create(singleReel);
            singleTopReel.position = (singleReel.position + reel.symbols.length - 1)  % reel.symbols.length;
            singleBottomReel.position = (singleReel.position + 1) % reel.symbols.length;

            if (index == 0) {
                viewTopReel += singleTopReel.display();
                viewReel += singleReel.display();
                viewBottonReel += singleBottomReel.display();

            } else {
                viewTopReel += ' | ' + singleTopReel.display();
                viewReel += ' | ' + singleReel.display();
                viewBottonReel += ' | ' + singleBottomReel.display();

            }
        });

        console.log(viewTopReel);
        console.log(viewReel);
        console.log(viewBottonReel);

    }
}

export class writter {

    constructor(name) {
        this.name = name;
    }

    print() {
        slotMachine.spin();
        slotMachine.display();
        slotMachine.spin();
        slotMachine.display();

    }
}


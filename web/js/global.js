Document.prototype.createElementFromString = function (str) {
    const element = new DOMParser().parseFromString(str, 'text/html');
    return element.documentElement.querySelector('body').firstChild;
};

var indexActor = 0;
var indexDirectory = 0;


// import("/js//js_cwicz_03.js").then((module) => {
//
//     //   new module.ExampleClass();
//     let writ = new module.writter('damian');
//     writ.print();
//     // import("/js//test.js").then((module) => {
//     //     writ.print();
//     // });
// });

window.onload = function() {
    var buttonActor = document.getElementById('form_addActor');
    var buttonDirector = document.getElementById('form_addDirector');

    if (buttonActor) {
        buttonActor.addEventListener("click", function () {
            indexActor++;
            var formActor = document.getElementById('form_actors');
            var actorPrototype = formActor.getAttribute('data-prototype');
            actorPrototype = actorPrototype.replace(/__name__/g, indexActor);
            actorPrototype = document.createElementFromString(actorPrototype);
            formActor.append(actorPrototype);
        });
    }

    if (buttonDirector) {
        buttonDirector.addEventListener("click", function () {
            indexDirectory++;
            var formDirector = document.getElementById('form_directors');
            var directorPrototype = formDirector.getAttribute('data-prototype');
            directorPrototype = directorPrototype.replace(/__name__/g, indexDirectory);
            directorPrototype = document.createElementFromString(directorPrototype);
            formDirector.append(directorPrototype);
        });
    }

};

// let timeToReload = 60000;
// timeToReload = timeToReload + (Math.random() * 10000);
// console.log(timeToReload);
// setTimeout(reloadPage, timeToReload);
//
// function reloadPage() {
//    location.reload();
// }

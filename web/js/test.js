const dayStart = "07:30";
const dayEnd = "17:45";

function scheduleMeeting(startTime, durationMinutes) {
    let endMeeting = startTime;
    let startMeeting = startTime.split(':');
    endMeeting = endMeeting.split(':');

    endMeeting[1] = Number(endMeeting[1]) + durationMinutes;
    endMeeting[0] = Number(endMeeting[0]) + Math.floor(endMeeting[1] / 60);

    endMeeting[1] %= 60;

    startMeeting[0] = String(startMeeting[0]).padStart(2, '0');
    endMeeting[0] = String(endMeeting[0]).padStart(2, '0');
    startMeeting[1] = String(startMeeting[1]).padStart(2, '0');
    endMeeting[1] = String(endMeeting[1]).padStart(2, '0');

    startMeeting = startMeeting.join(":");
    endMeeting = endMeeting.join(":");


    if (dayStart < startMeeting && endMeeting < dayEnd) {
        return true;
    } else {
        return false;
    }

}

export class writter {

    constructor(name) {
        this.name = name;
    }

    print() {


        //console.log(`hi ${this.name}`);
    }
}


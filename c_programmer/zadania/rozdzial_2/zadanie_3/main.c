#include <stdio.h>

int main(int argc, char *argv[])
{
  int wiek, dni_w_roku, wiek_w_dniach;
  wiek = 33;
  dni_w_roku = 365;
  
  wiek_w_dniach = wiek * dni_w_roku;
  printf("W wieku %d lat masz %d dni\n", wiek, wiek_w_dniach);
  system("PAUSE");	
  return 0;
}

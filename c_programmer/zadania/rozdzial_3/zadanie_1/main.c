#include <stdio.h>
#include <limits.h>

int main(int argc, char *argv[])
{
  int max_int = INT_MAX;
  double max_float = 1.0e20;
  double min_float = 1.0e-20;
  
  printf("int - %d po dodaniu 1 wynosi %d\n",max_int, max_int+1);
  printf("double - %f\n po pomnozeniu przez 1000 wynosi %f\n",max_float, max_float*1000.0f);
  printf("double - %.20f po podzieleniu przez 1000 wynosi %.20f\n",min_float, min_float/1000);

  
  
  system("PAUSE");	
  return 0;
}
